package com.teknei.bid.services;

import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidClieRegProc;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import com.teknei.bid.persistence.repository.BidRegProcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class ClientRegProcServices {

    @Autowired
    private BidRegProcRepository bidRegProcRepository;
    @Autowired
    private BidCurpRepository bidCurpRepository;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    private static final Logger log = LoggerFactory.getLogger(ClientRegProcServices.class);
    private static final String ESTA_PROC = "CAP-INI";

    public void openRegProc(Long operationId, String username){
    //log.info("lblancas : [tkn-service-client] :: "+this.getClass().getName()+".openRegProc");
        BidClieCurp clieCurp = bidCurpRepository.findTopByIdClie(operationId);
        BidClieRegProc regProc = new BidClieRegProc();
        regProc.setIdClie(operationId);
        regProc.setCurp(clieCurp.getCurp());
        regProc.setUsrCrea(username);
        regProc.setUsrOpeCrea(username);
        regProc.setFchCrea(new Timestamp(System.currentTimeMillis()));
        regProc.setIdEsta(1);
        regProc.setIdTipo(3);
        bidRegProcRepository.save(regProc);
    }

    public void updateStatus(Long idClient, String username) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".updateStatus");
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(ESTA_PROC, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi("client-api");
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }

}
