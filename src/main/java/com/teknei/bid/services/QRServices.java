package com.teknei.bid.services;

import com.teknei.bid.persistence.entities.BidClieQr;
import com.teknei.bid.persistence.repository.BidClieQrRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.UUID;

@Component
public class QRServices {

    @Autowired
    private BidClieQrRepository qrRepository;
    private static final Logger log = LoggerFactory.getLogger(QRServices.class);
    private static final int QR_STATUS_ACTIVE = 70201;
    private static final int QR_STATUS_USED = 70202;
    private static final int QR_STATUS_TIMED_OUT = 70203;
    private static final int QR_STATUS_CANCELED = 70204;
    public String generateQR(Long idCustomer, String usernameRequesting)
    { 
        //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".dataSource");
        BidClieQr clieQr = new BidClieQr();
        //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".dataSource");
        clieQr.setFchCrea(new Timestamp(System.currentTimeMillis()));
        clieQr.setIdClie(idCustomer);
        //TODO verify if fch_venc is set here or set by trigger in DB
        clieQr.setIdEsta(1);
        clieQr.setIdEstaQr(QR_STATUS_ACTIVE);
        clieQr.setIdTipo(3);
        clieQr.setQr(UUID.randomUUID().toString());
        clieQr.setUsrCrea(usernameRequesting);
        clieQr.setUsrOpeCrea(usernameRequesting);
        BidClieQr qrSaved = null;
        try {
            qrSaved = qrRepository.save(clieQr);
        } catch (Exception e) {
            log.error("Error saving QR for customer: {} with message: {}", idCustomer, e.getMessage());
            return null;
        }
        return qrSaved.getQr();

    }

    public long validateQR(String qr, String usernameRequesting)
    {
    	//log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".validateQR");
        BidClieQr qrLook = qrRepository.findByQrAndIdEstaQr(qr, QR_STATUS_ACTIVE);
        if(qrLook == null){
            return -1l;
        }
        qrLook.setIdEstaQr(QR_STATUS_USED);
        qrLook.setFchModi(new Timestamp(System.currentTimeMillis()));
        qrLook.setUsrModi(usernameRequesting);
        qrLook.setUsrOpeModi(usernameRequesting);
        try {
            qrRepository.save(qrLook);
        } catch (Exception e) {
            log.error("Could not update QR status for customer: {} with message: {}", qrLook.getIdClie(), e.getMessage());
            return 0l;
        }
        return qrLook.getIdClie();
    }

}
