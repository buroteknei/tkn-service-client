package com.teknei.bid.services;

import com.teknei.bid.dto.*;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class ClientService {
	
	private static final Logger log = LoggerFactory.getLogger(ClientService.class);
    @Autowired
    private BidCurpRepository bidCurpRepository;
    @Autowired
    private BidClieRepository bidClieRepository;
    @Autowired
    private BidMailRepository bidMailRepository;
    @Autowired
    private BidTelRepository bidTelRepository;
    @Autowired
    private BidCelRepository bidCelRepository;
    @Autowired
    private BidClieTipoRepository bidClieTipoRepository;
    @Autowired
    private BidIfeRepository bidIfeRepository;
    @Autowired
    private BidDireNestRepository bidDireNestRepository;
    @Autowired
    private BidTasRepository bidTasRepository;
    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidRegProcRepository bidRegProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidInstCredRepository bidInstCredRepository;
    @Autowired
    private BidClieCtaDestRepository bidClieCtaDestRepository;
    @Autowired
    private BidEmprRepository bidEmprRepository;
    @Autowired
    private BidEmprClieRepository bidEmprClieRepository;
    @Autowired
    private ClientService thisService;
    private static final String ESTA_PROC_FORM = "CAP-INI";
    private static final String ESTA_PROC_CTA_DEST = "CAP-CD";
 

    public boolean isValidEmprId(Long emprId) 
    {
    	//log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isValidEmprId");
        try {
            BidEmpr bidEmpr = bidEmprRepository.findByIdEmprAndIdEsta(emprId, 1);
            if (bidEmpr == null) {
                return false;
            }
            return true;
        } catch (Exception e) {
            log.error("Error finding valid company with message: {}", e.getMessage());
            return false;
        }
    }

    public List<BidInstCred> findAllInstCred() {
    	//log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findAllInstCred");
        List<BidInstCred> list = new ArrayList<>();
        try {
            list = bidInstCredRepository.findAll();
            return list;
        } catch (Exception e) {
            log.error("Error finding credit institutions with message: {}", e.getMessage());
        }
        return list;
    }

    public BidInstCred saveBidInstCred(BidCreditInstitutionRequest request) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".saveBidInstCred");
        try {
            BidInstCred bidInstCred = null;
            if (request.getId() == null || request.getId().equals(0l)) {
                bidInstCred = new BidInstCred();
                bidInstCred.setFchCrea(new Timestamp(System.currentTimeMillis()));
                bidInstCred.setUsrCrea(request.getUsername());
                bidInstCred.setIdEsta(1);
                bidInstCred.setIdTipo(3);
            } else {
                bidInstCred = bidInstCredRepository.findOne(request.getId());
                bidInstCred.setIdInstCred(request.getId());
                bidInstCred.setUsrModi(request.getUsername());
                bidInstCred.setFchModi(new Timestamp(System.currentTimeMillis()));
                if (request.getActive() == null || request.getActive().equals(false)) {
                    bidInstCred.setIdEsta(2);
                } else {
                    bidInstCred.setIdEsta(1);
                }
            }
            bidInstCred.setNomCort(request.getName());
            bidInstCred.setNomLarg(request.getDesc());
            return bidInstCredRepository.save(bidInstCred);
        } catch (Exception e) {
            log.error("Error saving credit institution with message: {}", e.getMessage());
            return null;
        }
    }

    public BidClieCtaDest saveBidClieCtaDest(BidClientAccountDestinyDTO request) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".saveBidClieCtaDest");
        try {
            BidClieCtaDest bidClieCtaDest = null;
            if (request.getNewIndicator() == null || request.getNewIndicator().equals(false)) {
                BidClieCtaDestPK pk = new BidClieCtaDestPK();
                pk.setCtaClabDest(request.getClabe());
                pk.setIdClie(request.getIdClient());
                bidClieCtaDest = bidClieCtaDestRepository.findOne(pk);
                bidClieCtaDest.setUsrModi(request.getUsername());
                bidClieCtaDest.setFchModi(new Timestamp(System.currentTimeMillis()));
                bidClieCtaDest.setUsrOpeModi(request.getUsername());
            } else {
                bidClieCtaDest = new BidClieCtaDest();
                bidClieCtaDest.setUsrCrea(request.getUsername());
                bidClieCtaDest.setFchCrea(new Timestamp(System.currentTimeMillis()));
                bidClieCtaDest.setIdTipo(3);
                bidClieCtaDest.setCtaClabDest(request.getClabe());
                bidClieCtaDest.setIdClie(request.getIdClient());
                bidClieCtaDest.setUsrOpeCrea(request.getUsername());
            }	
            
            bidClieCtaDest.setHolder(request.getHolder());
            bidClieCtaDest.setIdInstCred(request.getIdCreditInstitution());
            String alias=request.getAlias();
            if(alias.equals(null))
            	alias=" ";
            if(alias.length()<=0)
            	alias=" ";
            bidClieCtaDest.setAliaCta(alias);
            Long  amount =request.getAmmount();
            if(amount.equals(null))
            	amount=0L; 
            BigDecimal montMax = new BigDecimal(amount);
            montMax = montMax.movePointLeft(2);
            bidClieCtaDest.setMontMax(montMax);
            if (request.getActive() != null && request.getActive().equals(false)) {
                bidClieCtaDest.setIdEsta(2);
            } else {
                bidClieCtaDest.setIdEsta(1);
            }
            BidClieCtaDest ctaDestPeristed = bidClieCtaDestRepository.save(bidClieCtaDest);
            updateStatus(request.getIdClient(), ESTA_PROC_CTA_DEST, request.getUsername());
            return ctaDestPeristed;
        } catch (Exception e) {
            log.error("Error saving destiny account with message: {}", e.getMessage());
            return null;
        }
    }

    public List<BidClieCtaDest> findAllByClientAndStatus(Long idClie, Integer idEsta) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findAllByClientAndStatus");
        try {
            return bidClieCtaDestRepository.findAllByIdClieAndIdEsta(idClie, idEsta);
        } catch (Exception e) {
            log.error("Error finding destiny accounts for client: {} and status: {} with message: {}", idClie, idEsta, e.getMessage());
            return null;
        }
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void updateProcessStatus(Long idClient, String user) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".updateProcessStatus");
        updateStatus(idClient, ESTA_PROC_FORM, user);
    }

    @Transactional
    public void updateCurp(Long idClient, String curp) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".updateCurp");
        bidCurpRepository.updateCurp(curp, idClient);
    }

    public void updateMail(Long idClient, String mail) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".updateMail");
        BidClieMail bidClieMail = bidMailRepository.findTopByIdClie(idClient);
        bidClieMail.setEmai(mail);
        bidMailRepository.save(bidClieMail);
    }

    @Transactional
    public void updateTelephone(Long idClient, String telephone) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".updateTelephone");
        bidTelRepository.updateTel(idClient, telephone);
    }

    @Transactional
    public void updateCelphone(Long idClient, String telephone) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".updateCelphone");
        bidCelRepository.updateNumCel(idClient, telephone);
    }

    public List<BidIfeValidationDTO> validateCoincidences(BidIfeValidationRequestDTO requestDTO) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".validateCoincidences");
        if (requestDTO.getMrz() != null && !requestDTO.getMrz().isEmpty()) {
            if (requestDTO.getMrz().trim().equals("null")) {
                requestDTO.setMrz(null);
            }
        }
        if (requestDTO.getClavElec() != null && !requestDTO.getClavElec().isEmpty()) {
            if (requestDTO.getClavElec().trim().equals("null")) {
                requestDTO.setClavElec(null);
            }
        }
        if (requestDTO.getOcr() != null && !requestDTO.getOcr().isEmpty()) {
            if (requestDTO.getOcr().trim().equals("null")) {
                requestDTO.setOcr(null);
            }
        }
        List<BidIfeValidationDTO> list = new ArrayList<>();
        List<BidClieIfeIne> ineList = null;
        if (requestDTO.getClavElec() != null && !requestDTO.getClavElec().isEmpty()) {
            if (requestDTO.getOcr() != null && !requestDTO.getOcr().isEmpty()) {
                if (requestDTO.getMrz() != null && !requestDTO.getMrz().isEmpty()) {
                    ineList = bidIfeRepository.findByClavElecAndOcrAndMrz(requestDTO.getClavElec(), requestDTO.getOcr(), requestDTO.getMrz());
                } else {
                    ineList = bidIfeRepository.findByOcrAndClavElec(requestDTO.getOcr(), requestDTO.getClavElec());
                }
            } else {
                if (requestDTO.getMrz() != null && !requestDTO.getMrz().isEmpty()) {
                    ineList = bidIfeRepository.findByMrzAndClavElec(requestDTO.getMrz(), requestDTO.getClavElec());
                } else {
                    ineList = bidIfeRepository.findByClavElec(requestDTO.getClavElec());
                }
            }
        } else {
            if (requestDTO.getOcr() != null && !requestDTO.getOcr().isEmpty()) {
                if (requestDTO.getMrz() != null && !requestDTO.getMrz().isEmpty()) {
                    ineList = bidIfeRepository.findByMrzAndOcr(requestDTO.getMrz(), requestDTO.getOcr());
                } else {
                    ineList = bidIfeRepository.findByOcr(requestDTO.getOcr());
                }
            } else {
                if (requestDTO.getMrz() != null && !requestDTO.getMrz().isEmpty()) {
                    ineList = bidIfeRepository.findByMrz(requestDTO.getMrz());
                }
            }
        }
        ineList.forEach(c -> list.add(new BidIfeValidationDTO(c.getIdClie(), c.getNomb(), c.getApePate(), c.getApeMate(), c.getClavElec(), c.getOcr(), c.getMrz(), c.getCurp())));
        return list;
    }

    public boolean verifyBiometricStatus(Long operationId) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".verifyBiometricStatus");
        try {
            BidClieRegProc candidate = bidRegProcRepository.findTopByIdClieAndRegDact(operationId, true);
            if (candidate == null) {
                return false;
            }
            return true;
        } catch (Exception e) {
            log.error("Error finding verification biometric for: {} with message: {}", operationId, e.getMessage());
            return false;
        }
    }

    public List<CurpDTO> findOpenedProcessForContractToday() {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findOpenedProcessForContractToday");
        //TODO improve process searching for BidEstaProcRepository
        LocalDateTime localDateBefore = LocalDate.now().atStartOfDay();
        LocalDateTime localDateAfter = LocalDate.now().plusDays(1).atStartOfDay();
        Timestamp tsBefore = new Timestamp(localDateBefore.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        Timestamp tsAfter = new Timestamp(localDateAfter.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        List<BidClieRegProc> list = bidRegProcRepository.findAllByRegIneAndRegFaciAndRegDomiAndRegDactAndRegContAndFchRegIneBetween(true, true, true, true, true, tsBefore, tsAfter);
        if (list == null || list.isEmpty()) {
            return new ArrayList<>();
        }
        List<CurpDTO> returnList = processAndFormList(list);
        return returnList;
    }

    public List<CurpDTO> findOpenedProcessForToday() {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findOpenedProcessForToday");
        //TODO improve process searching for BidEstaProcRepository
        LocalDateTime localDateBefore = LocalDate.now().atStartOfDay();
        LocalDateTime localDateAfter = LocalDate.now().plusDays(1).atStartOfDay();
        Timestamp tsBefore = new Timestamp(localDateBefore.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        Timestamp tsAfter = new Timestamp(localDateAfter.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        List<BidClieRegProc> list = bidRegProcRepository.findAllByRegIneAndRegFaciAndRegDomiAndRegDactAndFchRegIneBetween(true, null, true, null, tsBefore, tsAfter);
        if (list == null || list.isEmpty()) {
            return new ArrayList<>();
        }
        List<CurpDTO> returnList = processAndFormList(list);
        return returnList;
    }

    private List<CurpDTO> processAndFormList(List<BidClieRegProc> listCandidates) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".processAndFormList");
        List<CurpDTO> returnList = new ArrayList<>();
        listCandidates.forEach(c -> returnList.add(processCandidateForList(c)));
        List<CurpDTO> listWithoutNulls = returnList.parallelStream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return listWithoutNulls;
    }

    private CurpDTO processCandidateForList(BidClieRegProc regProc) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".processCandidateForList");
        try {
            CurpDTO curpDTO = new CurpDTO(regProc.getIdClie(), regProc.getCurp());
            curpDTO.setScanId(bidScanRepository.findByIdRegi(regProc.getIdClie()).getScanId());
            curpDTO.setDocumentId(bidTasRepository.findByIdClie(regProc.getIdClie()).getIdTas());
            return curpDTO;
        } catch (Exception e) {
            log.error("Error processing candidate: {} with message: {}", regProc, e.getMessage());
            return null;
        }
    }

    /**
     * Use <code>findStepFromIdClient</code> instead
     *
     * @param id
     * @return
     */
    @Deprecated
    public Integer findStepFromReference(Long id) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findStepFromReference");
        BidClieCurp bidClieCurp = bidCurpRepository.findTopByIdClie(id);
        if (bidClieCurp == null) {
            return null;
        }
        return findStepFromReference(bidClieCurp.getCurp());
    }

    public Integer findStepFromCurp(String curp) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findStepFromCurp");
        try {
            BidClieCurp clieCurp = bidCurpRepository.findFirstByCurp(curp);
            return findStepFromIdClient(clieCurp.getIdClie());
        } catch (Exception e) {
            log.error("Error finding client from curp: {} with message_ {}", curp, e.getMessage());
            return 0;
        }
    }

    public Integer findStepFromIdClient(Long id) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findStepFromIdClient");
        try {
            List<BidClieRegEsta> listStatus = bidClieRegEstaRepository.findAllByIdClie(id);
            List<BidEstaProc> listActives = bidEstaProcRepository.findAllByIdEstaOrderByIdEstaProc(1);
            for (BidEstaProc c : listActives) {
                BidClieRegEsta estaCurrent = listStatus.stream().filter(p -> c.getIdEstaProc() == p.getIdEstaProc() && p.isEstaConf() == true).findAny().orElse(null);
                if (estaCurrent != null) {
                    return (int) c.getIdEstaProc();
                }
            }
            return 0;
        } catch (Exception e) {
            log.error("Error finding last status from: {} with message: {}", id, e.getMessage());
            return 0;
        }
    }

    @Deprecated
    public Integer findStepFromReference(String curp) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findStepFromReference");
        log.info("Searching for: {}", curp);
        BidClieRegProc regProc = bidRegProcRepository.findTopByCurp(curp);
        if (regProc == null) {
            return 0;
        }
        Integer step = 1;
        if (regProc.getRegIne() != null) {
            step = 2;
        }
        if (regProc.getRegFaci() != null) {
            step = 3;
        }
        if (regProc.getRegDomi() != null) {
            step = 4;
        }
        if (regProc.getRegDact() != null) {
            step = 5;
        }
        if (regProc.getRegCont() != null) {
            step = 6;
        }
        return step;
    }

    public DetailTSRecordDTO findTSDetailFromReference(Long id) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findTSDetailFromReference");
        BidClieCurp bidClieCurp = bidCurpRepository.findTopByIdClie(id);
        if (bidClieCurp == null) {
            return null;
        }
        return findTSDetailFromReference(bidClieCurp.getCurp());
    }

    public DetailTSRecordDTO findTSDetailFromReference(String curp) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findTSDetailFromReference");
        BidClieRegProc regProc = bidRegProcRepository.findTopByCurp(curp);
        if (regProc == null) {
            return null;
        }
        DetailTSRecordDTO detailTSRecordDTO = new DetailTSRecordDTO();
        detailTSRecordDTO.setAddress(regProc.getFchRegDomi());
        detailTSRecordDTO.setContract(regProc.getFchRegCont());
        detailTSRecordDTO.setCredentials(regProc.getFchRegIne());
        detailTSRecordDTO.setFacial(regProc.getFchRegFaci());
        detailTSRecordDTO.setFingers(regProc.getFchRegDact());
        detailTSRecordDTO.setCurp(curp);
        detailTSRecordDTO.setId(regProc.getIdClie());
        return detailTSRecordDTO;
    }

    public ClientDetailDTO findDetail(Long id) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findDetail");
        BidClieCurp bidClieCurp = bidCurpRepository.findTopByIdClie(id);
        if (bidClieCurp == null) {
            return null;
        }
        return findDetail(bidClieCurp.getCurp());
    }

    public ClientDetailDTO findDetailByMail(String mail) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findDetailByMail");
        log.info("Searching by detail mail: {}", mail);
        BidClieMail bidClieMail = bidMailRepository.findByEmai(mail);
        log.info("Mail result found: {}", bidClieMail);
        if (bidClieMail == null) {
            return null;
        }
        Long idClie = bidClieMail.getIdClie();
        log.info("Return found ok : {}", idClie);
        return findDetail(idClie);
    }

    public ClientDetailDTO findDetailByTel(String tel) {
    //log.info("lblancas : [tkn-service-client] :: "+this.getClass().getName()+".findDetailByTel");
        log.info("Searching by detail tel: {}", tel);
        BidTel bidTel = bidTelRepository.findTopByTele(tel);
        log.info("Tel found : {}", bidTel);
        if (bidTel == null) {
            return null;
        }
        Long idClie = bidTel.getIdClie();
        log.info("Tel found ok: {}", idClie);
        return findDetail(idClie);
    }

    public ClientDetailDTO findDetailByCel(String cel) {
    //log.info("lblancas : [tkn-service-client] :: "+this.getClass().getName()+".findDetailByCel");
        log.info("Searching by detail cel: {}", cel);
        BidCel bidCel = bidCelRepository.findTopByNumCel(cel);
        log.info("Cel found : {}", bidCel);
        if (bidCel == null) {
            return null;
        }
        Long idClie = bidCel.getIdClie();
        log.info("Tel found ok: {}", idClie);
        return findDetail(idClie);
    }

    public ClientDetailDTO findDetail(String curp) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findDetail");
        log.info("Searching by detail curp: {}", curp);
        BidClieCurp bidClieCurp = bidCurpRepository.findFirstByCurp(curp);
        if (bidClieCurp == null) {
            return null;
        }
        Long idClient = bidClieCurp.getIdClie();
        BidClie bidClie = bidClieRepository.findOne(idClient);
        //Find by idClie instead of composite key
        BidClieMail bidClieMail = bidMailRepository.findTopByIdClie(idClient);
        if (bidClieMail == null) {
            bidClieMail = bidMailRepository.findTopByIdClie(idClient);
        }
        List<BidCel> bidCelList = bidCelRepository.findAllByIdClieAndIdEsta(idClient, 1);
        BidClieIfeInePK bidClieIfeInePK = new BidClieIfeInePK();
        bidClieIfeInePK.setIdIfe(idClient);
        bidClieIfeInePK.setIdClie(idClient);
        BidClieIfeIne bidClieIfeIne = bidIfeRepository.findOne(bidClieIfeInePK);
        BidClieDireNestPK bidClieDireNestPK = new BidClieDireNestPK();
        bidClieDireNestPK.setIdDire(idClient);
        bidClieDireNestPK.setIdClie(idClient);
        BidClieDireNest bidClieDireNest = bidDireNestRepository.findOne(bidClieDireNestPK);
        BidClieTas bidTas = bidTasRepository.findByIdClie(idClient);
        BidScan bidScan = bidScanRepository.findByIdRegi(idClient);
        ClientDetailDTO clientDTO = new ClientDetailDTO();
        clientDTO.setId(idClient);
        clientDTO.setCurpDocument(curp);
        clientDTO.setCurp(curp);
        if (bidCelList != null && !bidCelList.isEmpty()) {
            List<ClieCelDTO> list = bidCelList.stream().map(t -> {
                ClieCelDTO clieCelDTO = new ClieCelDTO();
                clieCelDTO.setTel(t.getNumCel());
                return clieCelDTO;
            }).collect(Collectors.toList());
            clientDTO.setTelephones(list);
        }
        if (bidClie != null) {
            clientDTO.setUsername(bidClie.getUsrCrea());
            clientDTO.setSurnameLast(bidClie.getApeMate());
            clientDTO.setSurnameFirst(bidClie.getApePate());
            clientDTO.setName(bidClie.getNomClie());
        }
        if (bidScan != null) {
            clientDTO.setScanId(bidScan.getScanId());
        }
        if (bidTas != null) {
            clientDTO.setDocumentId(bidTas.getIdTas());
        }
        if (bidClieMail != null) {
            clientDTO.setEmail(bidClieMail.getEmai());
        }
        if (bidClieIfeIne != null) {
            clientDTO.setPersonalNumber(bidClieIfeIne.getClavElec());
            clientDTO.setMrz(bidClieIfeIne.getMrz());
            clientDTO.setOcr(bidClieIfeIne.getOcr());
            clientDTO.setVig(String.valueOf(bidClieIfeIne.getVige()));
        }
        if (bidClieDireNest != null) {
            clientDTO.setDire(bidClieDireNest.getDirNestDos());
        }
        List<BidClieTipo> listTypes = bidClieTipoRepository.findAllByIdClie(idClient);
        List<Integer> listTypesI = new ArrayList<>();
        if(listTypes != null && !listTypes.isEmpty()){
            listTypes.forEach(t -> listTypesI.add(t.getIdTipoClie()));
        }
        clientDTO.setIdClientType(listTypesI);
        return clientDTO;
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public ClientDTO saveInternalData(ClientDTO dto) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".saveInternalData");
        //1st step - save the client
        BidClie bidClie = new BidClie();
        bidClie.setApePate(dto.getSurnameFirst());
        bidClie.setApeMate(dto.getSurnameLast());
        bidClie.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidClie.setIdEsta(1);
        bidClie.setIdTipo(3);
        bidClie.setNomClie(dto.getName());
        bidClie.setUsrCrea(dto.getUsername());
        bidClie.setUsrOpeCrea(dto.getUsername());
        BidClie bidClieSaved = bidClieRepository.save(bidClie);
        Long idClient = bidClieSaved.getIdClie();
        //2nd step - save the curp
        BidClieCurp bidClieCurp = new BidClieCurp();
        bidClieCurp.setIdClie(idClient);
        bidClieCurp.setCurp(dto.getCurp());
        bidClieCurp.setUsrOpeCrea(dto.getUsername());
        bidClieCurp.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidClieCurp.setIdEsta(1);
        bidClieCurp.setIdTipo(3);
        bidClieCurp.setUsrCrea(dto.getUsername());
        bidCurpRepository.save(bidClieCurp);
        //3rd step - save the mail
        BidClieMail bidClieMail = new BidClieMail();
        bidClieMail.setEmai(dto.getEmail());
        bidClieMail.setIdClie(idClient);
        bidClieMail.setIdEmai(idClient);
        bidClieMail.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidClieMail.setUsrCrea(dto.getUsername());
        bidClieMail.setUsrOpeCrea(dto.getUsername());
        bidClieMail.setIdEsta(1);
        bidClieMail.setIdTipo(3);
        bidMailRepository.save(bidClieMail);
        //4th step - save the phones
        List<ClieCelDTO> telephones = dto.getTelephones();
        if(telephones != null && !telephones.isEmpty()){
            telephones.forEach(t -> saveCelPhone(t.getTel(), idClient, dto.getUsername()));
        }
        //5th step - save the client type
        BidClieTipo bidClieTipo = new BidClieTipo();
        bidClieTipo.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidClieTipo.setIdClie(idClient);
        if(dto.getIdClientType() != null && !dto.getIdClientType().isEmpty()){
            bidClieTipo.setIdTipoClie(dto.getIdClientType().get(0));
        }
        bidClieTipo.setUsrCrea(dto.getUsername());
        bidClieTipo.setIdEsta(1);
        bidClieTipo.setIdTipo(3);
        bidClieTipo.setUsrOpeCrea(dto.getUsername());
        bidClieTipoRepository.save(bidClieTipo);
        dto.setId(idClient);
        //6th step - assigns company to customer
        BidEmprClie bidEmprClie = new BidEmprClie();
        bidEmprClie.setIdClie(dto.getId());
        bidEmprClie.setIdEmpr(dto.getEmprId());
        bidEmprClie.setIdEsta(1);
        bidEmprClie.setIdTipo(3);
        bidEmprClie.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidEmprClie.setUsrCrea(dto.getUsername());
        bidEmprClie.setUsrOpeCrea(dto.getUsername());
        bidEmprClieRepository.save(bidEmprClie);
        return dto;
    }

    private void savePhone(String tel, int type, long idClient, String username){
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".savePhone");
        BidTel bidTel = new BidTel();
        bidTel.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidTel.setIdClie(idClient);
        bidTel.setIdEsta(1);
        bidTel.setTele(tel);
        bidTel.setUsrCrea(username);
        bidTel.setUsrOpeCrea(username);
        bidTelRepository.save(bidTel);
    }

    private void saveCelPhone(String tel, long idClient, String username){
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".saveCelPhone");
        BidCel bidCel = new BidCel();
        bidCel.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidCel.setIdClie(idClient);
        bidCel.setIdEsta(1);
        bidCel.setNumCel(tel);
        bidCel.setUsrCrea(username);
        bidCel.setUsrOpeCrea(username);
        bidCelRepository.save(bidCel);
    }

    public ClientDTO saveClientData(ClientDTO dto) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".saveClientData");
        ClientDTO savedInstance = thisService.saveInternalData(dto);
        thisService.updateProcessStatus(savedInstance.getId(), dto.getUsername());
        return savedInstance;
    }

    /**
     * Validates the existence of the data and its format
     *
     * @param curp - the curp to be validated
     * @return <pre>0 if correct</pre> <pre>1 if the curp is already on DB</pre><pre>2 if the curp is not on the DB but its not valid by regex</pre>
     */
    public int isCurpValid(String curp) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isCurpValid");
        boolean isAlreadyInDB = isCurpAlreadyInDB(curp);
        if (isAlreadyInDB) {
            return 1;
        }
        boolean isValidByRegex = isValidCurpByRegex(curp);
        if (!isValidByRegex) {
            return 2;
        }
        return 0;
    }

    public int isTelephoneValid(List<ClieTelDTO> tels) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isTelephoneValid");
        List<Integer> listValidations = tels.stream().map(t -> isTelephoneValid(t.getTel())).collect(Collectors.toList());
        if (listValidations.contains(1)) {
            return 1;
        }
        if (listValidations.contains(2)) {
            return 2;
        }
        return 0;
    }

    public int isMobilephoneValid(List<ClieCelDTO> tels) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isMobilephoneValid");
        List<Integer> listValidations = tels.stream().map(t -> isTelephoneValid(t.getTel())).collect(Collectors.toList());
        if (listValidations.contains(1)) {
            return 1;
        }
        if (listValidations.contains(2)) {
            return 2;
        }
        return 0;
    }

    /**
     * Validates the existence of the data and its format
     *
     * @param telephone - the telephone to be validated
     * @return <pre>0 if correct</pre> <pre>1 if the telephone is already on DB</pre><pre>2 if the telephone is not on the DB but its not valid by regex</pre>
     */
    public int isTelephoneValid(String telephone) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isTelephoneValid");
        boolean isAlreadyInDB = isAlreadyInDB(telephone);
        if (isAlreadyInDB) {
            return 1;
        }
        boolean isValidByRegex = isValidPhoneByRegex(telephone);
        if (!isValidByRegex) {
            return 2;
        }
        return 0;
    }

    /**
     * Validates the existence of the data and its format
     *
     * @param telephone - the telephone to be validated
     * @return <pre>0 if correct</pre> <pre>1 if the telephone is already on DB</pre><pre>2 if the telephone is not on the DB but its not valid by regex</pre>
     */
    public int isMobilephoneValid(String telephone) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isMobilephoneValid");
        boolean isAlreadyInDB = isAlreadyInDBCel(telephone);
        if (isAlreadyInDB) {
            return 1;
        }
        boolean isValidByRegex = isValidPhoneByRegex(telephone);
        if (!isValidByRegex) {
            return 2;
        }
        return 0;
    }


    /**
     * Validates the existence of the data and its format
     *
     * @param mail - the email to be validated
     * @return <pre>0 if correct</pre> <pre>1 if the email is already on DB</pre><pre>2 if the email is not on the DB but its not valid by regex</pre>
     */
    public int isEmailValid(String mail) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isEmailValid");
        boolean isAlreadyInDB = isEmailAlreadyInDB(mail);
        if (isAlreadyInDB) {
            return 1;
        }
        boolean isValidByRegex = isEmailValidByRegex(mail);
        if (!isValidByRegex) {
            return 2;
        }
        return 0;
    }

    private boolean isAlreadyInDB(String tel) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isAlreadyInDB");
        try {
            BidTel bidTel = bidTelRepository.findTopByTele(tel);
            if (bidTel == null) {
                return false;
            }
            return true;
        } catch (Exception e) {
            log.error("Error finding previous telephone by: {} with message: {}", tel, e.getMessage());
        }
        return true;
    }

    private boolean isAlreadyInDBCel (String tel) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isAlreadyInDBCel");
        try {
            BidCel bidCel = bidCelRepository.findTopByNumCel(tel);
            if (bidCel == null) {
                return false;
            }
            return true;
        } catch (Exception e) {
            log.error("Error finding previous telephone by: {} with message: {}", tel, e.getMessage());
        }
        return true;
    }

    private boolean isValidPhoneByRegex(String phone) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isValidPhoneByRegex");
        //TODO
        return true;
    }

    private boolean isEmailAlreadyInDB(String mail) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isEmailAlreadyInDB");
        try {
            BidClieMail bidClieMail = bidMailRepository.findTopByEmai(mail);
            if (bidClieMail == null) {
                return false;
            }
            return true;
        } catch (Exception e) {
            log.error("Error finding previous mail for: {} with error message: {}", mail, e.getMessage());
        }
        return true;
    }

    private boolean isEmailValidByRegex(String email) {
        //TODO
        return true;
    }

    private boolean isValidCurpByRegex(String curp) {
        //TODO
        return true;
    }

    private boolean isCurpAlreadyInDB(String curp) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".isCurpAlreadyInDB");
        try {
            BidClieCurp bidClieCurp = bidCurpRepository.findFirstByCurp(curp);
            if (bidClieCurp == null) {
                return false;
            }
            return true;
        } catch (Exception e) {
            log.error("Error finding coincidences for curp: {} with message: {}", curp, e.getMessage());
            return true;
        }
    }

    private void updateStatus(Long idClient, String status, String userModi) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".updateStatus");
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(status, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi("client-api");
            regEsta.setUsrOpeModi(userModi);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }
}
