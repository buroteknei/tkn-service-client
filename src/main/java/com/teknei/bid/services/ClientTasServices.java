package com.teknei.bid.services;

import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.entities.BidClieTas;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.persistence.repository.BidTasRepository;
import com.teknei.bid.util.tas.TasManager;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Service
public class ClientTasServices {
	
	private static final Logger log = LoggerFactory.getLogger(ClientTasServices.class);
    @Autowired
    private TasManager tasManager;
    @Value("${tkn.tas.casefile}")
    private String tasCasefile;
    @Value("${tkn.tas.name}")
    private String tasName;
    @Value("${tkn.tas.surname}")
    private String tasSurname;
    @Value("${tkn.tas.lastname}")
    private String tasLastname;
    @Value("${tkn.tas.identification}")
    private String tasIdentification;
    @Value("${tkn.tas.date}")
    private String tasDate;
    @Value("${tkn.tas.scan}")
    private String tasScan;
    @Value("${tkn.tas.id}")
    private String tasId;
    @Autowired
    private BidClieRepository bidClieRepository;
    @Autowired
    private BidTasRepository bidTasRepository;

    public String initCaseFile(String user, Long operationId, String username) throws Exception {
    	//log.info("lblancas : [tkn-service-client] :: "+this.getClass().getName()+".initCaseFile");
        Map<String, String> docProperties = new HashMap<>();
        BidClie bidClie = bidClieRepository.findOne(operationId);
        docProperties.put(tasName, bidClie.getNomClie());
        docProperties.put(tasSurname, bidClie.getApePate());
        docProperties.put(tasLastname, bidClie.getApeMate());
        docProperties.put(tasIdentification, String.valueOf(operationId));
        String dateISO8601 = ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT).toString();
        docProperties.put(tasDate, dateISO8601);
        docProperties.put(tasScan, String.valueOf(operationId));
        docProperties.put(tasId, String.valueOf(operationId));
        JSONObject expediente = tasManager.addDocument(tasCasefile, null, null, docProperties, null, null, null);
        String id = expediente.get("id").toString();
        BidClieTas bidTas = new BidClieTas();
        bidTas.setIdClie(operationId);
        bidTas.setIdTas(id);
        bidTas.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidTas.setUsrCrea(user);
        bidTas.setUsrOpeCrea(username);
        bidTasRepository.save(bidTas);
        return id;
    }

    public String findCasefile(String user, Long operationId){
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findCasefile");
        BidClieTas bidTas = bidTasRepository.findByIdClie(operationId);
        return bidTas.getIdTas();
    }
}
