package com.teknei.bid.services;

import com.teknei.bid.dto.BidDocuRelDTO;
import com.teknei.bid.persistence.entities.BidDocuPrim;
import com.teknei.bid.persistence.entities.BidDocuSecu;
import com.teknei.bid.persistence.entities.BidRelDocu;
import com.teknei.bid.persistence.repository.BidDocuPrimRepository;
import com.teknei.bid.persistence.repository.BidDocuSecuRepository;
import com.teknei.bid.persistence.repository.BidRelDocuRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientDocumentServices {

    private static final Logger log = LoggerFactory.getLogger(ClientDocumentServices.class);
    @Autowired
    private BidDocuPrimRepository bidDocuPrimRepository;
    @Autowired
    private BidDocuSecuRepository bidDocuSecuRepository;
    @Autowired
    private BidRelDocuRepository bidRelDocuRepository;

    public List<BidDocuRelDTO> findActiveRelations() {
    //log.info("lblancas : [tkn-service-client] :: "+this.getClass().getName()+".findActiveRelations");
        List<BidDocuRelDTO> list = new ArrayList<>();
        try {
            List<Long> distinctsRelationsActiveIds = bidRelDocuRepository.findDistinctIdDocuPrim(1);
            distinctsRelationsActiveIds.forEach(p -> {
                BidDocuPrim prim = bidDocuPrimRepository.findOne(p);
                BidDocuRelDTO dto = new BidDocuRelDTO(p, prim.getCodDocuPrim(), prim.getDescDocuPrim(), false, false);
                List<BidRelDocu> listRel = bidRelDocuRepository.findAllByIdEstaAndIdDocuPrim(1, p);
                List<BidDocuRelDTO> children = new ArrayList<>();
                listRel.forEach(l -> {
                    BidDocuSecu secu = bidDocuSecuRepository.findOne((long) l.getIdDocuSecu());
                    children.add(new BidDocuRelDTO(secu.getIdDocuSecu(), secu.getCodDocuSecu(), secu.getDescDocuSecu(), secu.isBolScanAnve(), secu.isBolScanReve()));
                });
                dto.setChildren(children);
                list.add(dto);
            });
        } catch (Exception e) {
            log.error("Error finding relationships in docs with messages: {}", e.getMessage());
        }
        return list;
    }

}
