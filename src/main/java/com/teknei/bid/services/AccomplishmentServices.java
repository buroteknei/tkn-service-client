package com.teknei.bid.services;

import com.teknei.bid.dto.StepStatusDTO;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.entities.BidTipoPlat;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import com.teknei.bid.persistence.repository.BidTipoPlatRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AccomplishmentServices {

    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    @Autowired
    private BidTipoPlatRepository bidTipoPlatRepository;
    private Map<Long, BidEstaProc> mapStatus;
    private Map<Long, BidTipoPlat> mapPlatforms;
    private static final Logger log = LoggerFactory.getLogger(AccomplishmentServices.class);

    @PostConstruct
    private void postConstruct() {
    //log.info("lblancas : [tkn-service-client] :: "+this.getClass().getName()+".postConstruct");
        mapStatus = new HashMap<>();
        mapPlatforms = new HashMap<>();
        fillMapPlatforms();
        fillMapStatus();
    }

    private void fillMapPlatforms() {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".fillMapPlatforms");
        try {
            bidTipoPlatRepository.findAllByIdEsta(1).forEach(p -> {
                mapPlatforms.put(p.getIdTipoPlat(), p);
            });
        } catch (Exception e) {
            log.error("Error filling platforms map with message: {}", e.getMessage());
        }
    }

    private void fillMapStatus() {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".fillMapStatus");
        try {
            bidEstaProcRepository.findAllByIdEstaOrderByIdEstaProc(1).forEach(c -> mapStatus.put(c.getIdEstaProc(), c));
        } catch (Exception e) {
            log.error("Error filling Status map with message: {}", e.getMessage());
        }
    }

    public List<StepStatusDTO> findRelated(Long idClient) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findRelated");
        List<StepStatusDTO> listAccomplishment = new ArrayList<>();
        try {
            List<BidClieRegEsta> clientAll = bidClieRegEstaRepository.findAllByIdClie(idClient);
            clientAll.forEach(c -> {
                StepStatusDTO step = new StepStatusDTO();
                step.setAccomplished(c.isEstaConf());
                step.setCodEstaProc(mapStatus.get(c.getIdEstaProc()).getCodEstaProc());
                step.setDescEstaProc(mapStatus.get(c.getIdEstaProc()).getDescEstaProc());
                step.setPlatformAccomplished(c.getIdTipoPlat() == null ? "NA" : mapPlatforms.get(c.getIdTipoPlat()).getCodTipoPlat());
                step.setTimestamp(c.getFchModi() == null ? 0l : c.getFchModi().getTime());
                step.setUserAccomplished("TODO");
                listAccomplishment.add(step);
            });
        } catch (Exception e) {
            log.error("Error finding accomplishment for customer: {} with message: {}", idClient, e.getMessage());
        }
        return listAccomplishment;
    }


}
