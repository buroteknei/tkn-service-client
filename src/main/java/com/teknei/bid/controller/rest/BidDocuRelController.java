package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.BidDocuRelDTO;
import com.teknei.bid.services.ClientDocumentServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/clieDocs")
public class BidDocuRelController {

    private static final Logger log = LoggerFactory.getLogger(BidDocuRelController.class);

    @Autowired
    private ClientDocumentServices services;

    @RequestMapping(value = "/findRelationsActive", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidDocuRelDTO>> findActiveDocumentRelations() {
    //log.info("lblancas : [tkn-service-client] :: "+this.getClass().getName()+".findActiveDocumentRelations");
        try {
            return new ResponseEntity<>(services.findActiveRelations(), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding document relations with message: {}", e.getMessage());
            return new ResponseEntity<>((List<BidDocuRelDTO>) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

}
