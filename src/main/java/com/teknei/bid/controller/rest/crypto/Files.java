package com.teknei.bid.controller.rest.crypto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.util.Base64;

public class Files {
	private static final Logger log = LoggerFactory.getLogger(Files.class);
	/**
	 * 
	 * @param key
	 * @param path
	 * @param nameFile
	 * @return
	 */
	public boolean createFileKey(byte key [], String path, String nameFile) {
		//log.info("lblancas : [tkn-service-client] :: "+this.getClass().getName()+".createFileKey");
	        FileWriter fichero = null;
	        PrintWriter pw = null;
	        try
	        {
	            fichero = new FileWriter(path + nameFile);
	            pw = new PrintWriter(fichero);
	            String asB64 = Base64.getEncoder().encodeToString(key);
	            pw.println(asB64);
	            
	            return true;
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	            return false;
	        } finally {
	           try {
	           if (null != fichero)
	              fichero.close();
	           } catch (Exception e2) {
	              e2.printStackTrace();
	           }
	        }
	}
	
	/**
	 * 
	 * @param pathKeyPub
	 * @return
	 */
	public byte [] readFileKey(String pathKeyPub) {
	//log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".readFileKey");
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;

		try {
			
			archivo = new File (pathKeyPub);
			fr = new FileReader (archivo);
			br = new BufferedReader(fr);
			
			StringBuffer buffer = new StringBuffer();
			String linea;
			while((linea=br.readLine())!=null) {
				buffer.append(linea);
			}
			
            byte[] asBytes = Base64.getDecoder().decode(buffer.toString());
			return asBytes;
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			try{                    
				if( null != fr ){   
					fr.close();     
				}                  
			}catch (Exception e2){ 
				e2.printStackTrace();
			}
		}

	}
	
	

}
