package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.ClieQRDTO;
import com.teknei.bid.dto.ClieQRDTORequest;
import com.teknei.bid.dto.ClientDetailDTO;
import com.teknei.bid.services.ClientService;
import com.teknei.bid.services.QRServices;
import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.javase.QRCode;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

@RestController
@RequestMapping(value = "/clientQR")
public class BidClieQRController {

    private static final Logger log = LoggerFactory.getLogger(BidClieQRController.class);

    @Value("${tkn.public.ip}")
    private String publicIP;
    @Value("${tkn.public.port}")
    private String publicPort;
    @Value("${tkn.public.applicationName}")
    private String publicBaseName;
    @Value("${tkn.public.applicationPath}")
    private String publicPath;
    @Autowired
    private QRServices qrServices;
    @Autowired
    private ClientService clientService;


    @RequestMapping(value = "/qr", method = RequestMethod.POST)
    public ResponseEntity<ClieQRDTO> generateQRToCotinueWithBiometric(@RequestBody ClieQRDTORequest qrdtoRequest){
    //log.info("lblancas : [tkn-service-client] :: "+this.getClass().getName()+".generateQRToCotinueWithBiometric");
        //TODO generate DB insert with QR data
        String qrGenerated = qrServices.generateQR(Long.valueOf(qrdtoRequest.getIdClient()), qrdtoRequest.getUsername());
        if(qrGenerated == null){
            return new ResponseEntity<>((ClieQRDTO) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        ClieQRDTO qrdto = new ClieQRDTO();
        try {
            String url = new StringBuilder("http://")
                    .append(publicIP)
                    .append(":")
                    .append(publicPort)
                    .append(publicBaseName)
                    .append(publicPath)
                    .append("/")
                    .append("customerQR")
                    .append("/")
                    .append(qrGenerated)
                    .toString();
            ByteArrayOutputStream stream = QRCode.from(url).to(ImageType.JPG).stream();
            byte[] bytes = stream.toByteArray();
            String qrb64 = Base64Utils.encodeToString(bytes);
            qrdto.setQr(qrb64);
            return new ResponseEntity<>(qrdto, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error generating QR code with message: {}", e.getMessage());
            return new ResponseEntity<>(qrdto, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/qr", method = RequestMethod.PUT)
    public ResponseEntity<String> validateQR(@RequestBody ClieQRDTO qrdto){
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".validateQR");
        long idClieQr = qrServices.validateQR(qrdto.getQr(), qrdto.getObs());
        JSONObject jsonObject = new JSONObject();
        if(idClieQr > 0){
            ClientDetailDTO detailDTO = clientService.findDetail(idClieQr);
            jsonObject.put("status", "OK");
            jsonObject.put("operationId", detailDTO.getId());
            jsonObject.put("curp", detailDTO.getCurp());
            jsonObject.put("empr", detailDTO.getEmprId());
            jsonObject.put("email", detailDTO.getEmail());
            jsonObject.put("mrz", detailDTO.getMrz());
            jsonObject.put("ocr", detailDTO.getOcr());
            jsonObject.put("address", detailDTO.getDire());
            jsonObject.put("name", detailDTO.getName());
            jsonObject.put("surnameFirst", detailDTO.getSurnameFirst());
            jsonObject.put("surnameLast", detailDTO.getSurnameLast());
            jsonObject.put("customerTypes", detailDTO.getIdClientType());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
        }else{
            jsonObject.put("status", "error");
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.NOT_FOUND);
        }
    }

}
