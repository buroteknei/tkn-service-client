package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.*;
import com.teknei.bid.persistence.entities.BidClieCtaDest;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.entities.BidInstCred;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import com.teknei.bid.services.AccomplishmentServices;
import com.teknei.bid.services.ClientRegProcServices;
import com.teknei.bid.services.ClientService;
import com.teknei.bid.services.ClientTasServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/client")
public class BidClieController {

    @Autowired
    private ClientService clientService;
    @Autowired
    private ClientTasServices clientTasServices;
    @Autowired
    private ClientRegProcServices clientRegProcServices;
    @Autowired
    private AccomplishmentServices accomplishmentServices;

    /*Injections from credential-command*/
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    private static final String ESTA_PROC = "CAP-CRE";
    /*End injections from credential-command*/
    private static final Logger log = LoggerFactory.getLogger(BidClieController.class);

    @ApiOperation(value = "Finds the accounts related to the customer")
    @RequestMapping(value = "/accountDestiny/{idClient}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidClieCtaDest>> findRelatedAccounts(@PathVariable Long idClient) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findRelatedAccounts idClient:"+idClient);
        List<BidClieCtaDest> list = clientService.findAllByClientAndStatus(idClient, 1);
        if (list == null || list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @ApiOperation(value = "Finds the accounts related to the customer, only inactive ones")
    @RequestMapping(value = "/accountDestinyInactive/{idClient}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidClieCtaDest>> findRelatedAccountsInactive(@PathVariable Long idClient) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findRelatedAccountsInactive idClient:"+idClient);
        List<BidClieCtaDest> list = clientService.findAllByClientAndStatus(idClient, 2);
        if (list == null || list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @ApiOperation(value = "Saves or updates account destiny information for the related customer. Identifiers are required, boolean fields are recommended for expected behavior", response = BidClieCtaDest.class)
    @RequestMapping(value = "/accountDestiny", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BidClieCtaDest> saveCtaDest(@RequestBody BidClientAccountDestinyDTO request) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".saveCtaDest request:");
        BidClieCtaDest clieCtaDest = clientService.saveBidClieCtaDest(request);
        if (clieCtaDest == null) {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<>(clieCtaDest, HttpStatus.OK);
    }

    @ApiOperation(value = "Gets all credit institution records", response = List.class)
    @RequestMapping(value = "/creditInstitution", method = RequestMethod.GET)
    public ResponseEntity<List<BidInstCred>> findInstCred() {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findInstCred");
        List<BidInstCred> list = clientService.findAllInstCred();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @ApiOperation(value = "Saves or updates new credit institution based on request values. Send only name values when new record is needed, all fields are required when update is request", response = BidInstCred.class)
    @RequestMapping(value = "/creditInstitution", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BidInstCred> saveInstCred(@RequestBody BidCreditInstitutionRequest request) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".saveInstCred");
        BidInstCred instCred = clientService.saveBidInstCred(request);
        if (instCred == null) {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<>(instCred, HttpStatus.OK);
    }

    @ApiOperation(value = "Finds the valid information based on the request", notes = "Response should contain only one record in the array", response = BidIfeValidationRequestDTO.class)
    @RequestMapping(value = "/validate/information/ine", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidIfeValidationDTO>> findValidation(@RequestBody BidIfeValidationRequestDTO validationRequestDTO) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findValidation");
        List<BidIfeValidationDTO> list = clientService.validateCoincidences(validationRequestDTO);
        if (list != null && !list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.OK);
        }
        return new ResponseEntity<>(list, HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Verifies if the customer has the biometric step completed", notes = "Returns 200 if ok, 404 if not found", response = Boolean.class)
    @RequestMapping(value = "/verify/biometric/{operationId}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> findVerificationForCandidate(@PathVariable Long operationId) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findVerificationForCandidate");
        boolean found = clientService.verifyBiometricStatus(operationId);
        if (found) {
            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Adds the specified customer to the database", notes = "The validation processes are attached as an internal json in the response field 'obs'", response = ClientDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the customer could be saved"),
            @ApiResponse(code = 422, message = "If the customer fails in the validation process"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<ClientDTO> addClient(@RequestBody ClientDTO clientDTO) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".addClient");
        log.info("Adding customer with data: {}", clientDTO);
        boolean isValidCompany = clientService.isValidEmprId(clientDTO.getEmprId());
        int curpValidationStatus = clientService.isCurpValid(clientDTO.getCurp());
        JSONObject jsonObject = new JSONObject();
        if (!isValidCompany) {
            ClientDTO rejectedByEmprId = new ClientDTO();
            rejectedByEmprId.setEmprId(clientDTO.getEmprId());
            jsonObject.put("emprIdValidation", false);
            rejectedByEmprId.setObs(jsonObject.toString());
            return new ResponseEntity<>(rejectedByEmprId, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        if (curpValidationStatus == 1) {
            ClientDTO rejectedByCurp = new ClientDTO();
            rejectedByCurp.setCurp(clientDTO.getCurp());
            jsonObject.put("curpValidation", curpValidationStatus);
            rejectedByCurp.setObs(jsonObject.toString());
            return new ResponseEntity<>(rejectedByCurp, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        int mailValidationStatus = clientService.isEmailValid(clientDTO.getEmail());
        int phoneValidationStatus = clientService.isMobilephoneValid(clientDTO.getTelephones());
        if (mailValidationStatus != 0) {
            jsonObject.put("emailValidation", mailValidationStatus);
        }
        if (phoneValidationStatus != 0) {
            jsonObject.put("phoneValidation", phoneValidationStatus);
        }
        try {
            ClientDTO savedInstance = clientService.saveClientData(clientDTO);
            savedInstance.setObs(jsonObject.toString());
            clientRegProcServices.updateStatus(savedInstance.getId(), clientDTO.getUsername());
            return new ResponseEntity<>(savedInstance, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error saving data for customer: {} with message: {}", clientDTO, e.getMessage());
            return new ResponseEntity<>(clientDTO, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Updates the customer record based on requested values. The id must be a valid identifier", notes = "If error ocurrs, the detail of the error will be a json in 'obs' field inside the response", response = ClientDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the customer could be updated"),
            @ApiResponse(code = 422, message = "If the customer fails in the update process"),
            @ApiResponse(code = 400, message = "Bad request, usually 'id' is missing")
    })
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<ClientDTO> updateClient(@RequestBody ClientDTO clientDTO) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".updateClient");
        Long idClient = clientDTO.getId();
        if (idClient == null) {
            return new ResponseEntity<>(clientDTO, HttpStatus.BAD_REQUEST);
        }
        String requestedPhone = null;
        if(clientDTO.getTelephones() != null && !clientDTO.getTelephones().isEmpty()){
            requestedPhone = clientDTO.getTelephones().get(0).getTel();
        }
        String requestedCurp = clientDTO.getCurp();
        String requestedMail = clientDTO.getEmail();
        boolean error = false;
        JSONObject jsonObject = new JSONObject();
        if (requestedCurp != null && !requestedCurp.isEmpty()) {
            try {
                clientService.updateCurp(idClient, requestedCurp);
            } catch (Exception e) {
                jsonObject.put("errorCurp", true);
                log.error("Error updating curp for request: {} with message: {}", clientDTO, e.getMessage());
                error = true;
            }
        }
        if (requestedMail != null && !requestedMail.isEmpty()) {
            try {
                clientService.updateMail(idClient, requestedMail);
            } catch (Exception e) {
                jsonObject.put("errorMail", true);
                log.error("Error updating mail for request: {} with message: {}", clientDTO, e.getMessage());
                error = true;
            }
        }
        if (requestedPhone != null && !requestedPhone.isEmpty()) {
            try {
                clientService.updateCelphone(idClient, requestedPhone);
            } catch (Exception e) {
                jsonObject.put("errorPhone", true);
                log.error("Error updating phone for request: {} with message: {}", clientDTO, e.getMessage());
                error = true;
            }
        }
        if (error) {
            clientDTO.setObs(jsonObject.toString());
            return new ResponseEntity<>(clientDTO, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        //update status
        updateStatus(idClient, clientDTO.getUsername());
        return new ResponseEntity<>(clientDTO, HttpStatus.OK);
    }

    @ApiOperation(value = "Finds the timestamps details related to the enrolment process for the given customer", response = DetailTSRecordDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the resource is found"),
            @ApiResponse(code = 404, message = "If the customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = {"/ts/curp/{curp}", "/ts/id/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<DetailTSRecordDTO> findDetailTS(@PathVariable Optional<String> curp, @PathVariable Optional<Long> id) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findDetailTS");
        DetailTSRecordDTO detailTSRecordDTO = null;
        try {
            if (curp.isPresent()) {
                detailTSRecordDTO = clientService.findTSDetailFromReference(getOptional(curp));
            } else {
                detailTSRecordDTO = clientService.findTSDetailFromReference(id.get());
            }
            if (detailTSRecordDTO == null) {
                return new ResponseEntity<>(detailTSRecordDTO, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(detailTSRecordDTO, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding TS detail for reference: {} , {} with message:", curp.get(), id.get(), e.getMessage());
            return new ResponseEntity<>(detailTSRecordDTO, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Open casefile in document manager remote API", response = Boolean.class)
    @RequestMapping(value = "/initCasefile", method = RequestMethod.POST)
    public ResponseEntity<Boolean> openCasefile(@RequestBody OpenCasefileDTO openCasefileDTO) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".openCasefile");
        try {
            clientTasServices.initCaseFile(openCasefileDTO.getUsername(), openCasefileDTO.getOperationId(), openCasefileDTO.getUserOpeCrea());
            clientRegProcServices.updateStatus(openCasefileDTO.getOperationId(), openCasefileDTO.getUserOpeCrea());
            return new ResponseEntity<>(true, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error opening casefile for: {} with message: {}", openCasefileDTO, e.getMessage());
            return new ResponseEntity<>(false, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Checks whether the casefile is valid and opened or not", response = String.class)
    @RequestMapping(value = "/checkCasefile", method = RequestMethod.POST)
    public ResponseEntity<String> findCasefile(@RequestBody OpenCasefileDTO openCasefileDTO) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findCasefile");
        try {
            String tasId = clientTasServices.findCasefile(openCasefileDTO.getUsername(), openCasefileDTO.getOperationId());
            return new ResponseEntity<>(tasId, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding opened casefile for: {} with message: {}", openCasefileDTO, e.getMessage());
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Finds the detail related to the given ID or CURP", response = ClientDetailDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the information could be found"),
            @ApiResponse(code = 404, message = "No information found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = {"/detail/id/{id}", "/detail/curp/{curp}", "/detail/mail/{mail}", "/detail/tel/{tel}"}, method = RequestMethod.GET)
    public ResponseEntity<ClientDetailDTO> findDetail(@PathVariable Optional<Long> id, @PathVariable Optional<String> curp, @PathVariable Optional<String> mail, @PathVariable Optional<String> tel) {
        //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findDetail");
        try {
            ClientDetailDTO detailDTO = null;
            if (id.isPresent()) {
                detailDTO = clientService.findDetail(id.get());
            } else if(curp != null && curp.isPresent()){
                log.info("Search by curp: {}", curp.get());
                detailDTO = clientService.findDetail(curp.get());
            }else if(mail != null && mail.isPresent()){
                log.info("Search by mail: {}", mail.get());
                String base64 = mail.get();
                byte[] encoded = Base64Utils.decodeFromString(base64);
                String mailDecoded = new String(encoded);
                detailDTO = clientService.findDetailByMail(mailDecoded);
            }else if(tel != null && tel.isPresent()){
                log.info("Search by tel: {}", tel.get());
                detailDTO = clientService.findDetailByCel(tel.get());
            }else{
                return new ResponseEntity<>((ClientDetailDTO) null, HttpStatus.NOT_FOUND);
            }
            if (detailDTO == null) {
                return new ResponseEntity<>((ClientDetailDTO) null, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(detailDTO, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            log.error("Error, data not found as requested, id: {} , curp: {}", id.get(), curp.get());
            return new ResponseEntity<>((ClientDetailDTO) null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Error finding detail with message: {}", e.getMessage());
            log.error("Error trace: ", e);
            return new ResponseEntity<>((ClientDetailDTO) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Finds the status map for accomplishment steps related to the enrollment process")
    @RequestMapping(value = "/accomplishment/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<StepStatusDTO>> findAccomplishmnet(@PathVariable Long id) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findAccomplishmnet");
        List<StepStatusDTO> listSteps = accomplishmentServices.findRelated(id);
        if (listSteps == null || listSteps.isEmpty()) {
            return new ResponseEntity<>(listSteps, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(listSteps, HttpStatus.OK);
    }

    @Deprecated
    @ApiOperation(value = "Finds the current step of the process for the given ID or CURP", response = Integer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the information could be found"),
            @ApiResponse(code = 404, message = "No information found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = {"/step/id/{id}", "/step/curp/{curp}"}, method = RequestMethod.GET)
    public ResponseEntity<Integer> findStepFromReference(@PathVariable Optional<Long> id, @PathVariable Optional<String> curp) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findStepFromReference");
        try {
            Integer step = null;
            if (id.isPresent()) {
                Long toSearch = id.get();
                step = clientService.findStepFromIdClient(toSearch);
            } else {
                String toSearch = getOptional(curp);
                step = clientService.findStepFromCurp(toSearch);
            }
            if (step == null) {
                return new ResponseEntity<>(0, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(step, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding detail for step with references: {} , {} and message: {}", id.get(), curp.get(), e.getMessage());
            return new ResponseEntity<>(-1, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/findProcesses/contractsForToday", method = RequestMethod.GET)
    public ResponseEntity<List<CurpDTO>> findOpenForContractProcess() {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findOpenForContractProcess");
        try {
            List<CurpDTO> listOpened = clientService.findOpenedProcessForContractToday();
            if (listOpened == null || listOpened.isEmpty()) {
                return new ResponseEntity<>(listOpened, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(listOpened, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding openRecords for today with message: {}", e.getMessage());
            return new ResponseEntity<>((List<CurpDTO>) null, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/findProcesses/openedForToday", method = RequestMethod.GET)
    public ResponseEntity<List<CurpDTO>> findOpenProcess() {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".findOpenProcess");
        try {
            List<CurpDTO> listOpened = clientService.findOpenedProcessForToday();
            if (listOpened == null || listOpened.isEmpty()) {
                return new ResponseEntity<>(listOpened, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(listOpened, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding openRecords for today with message: {}", e.getMessage());
            return new ResponseEntity<>((List<CurpDTO>) null, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/openRegProc", method = RequestMethod.POST)
    public ResponseEntity<Boolean> openRegProc(@RequestBody OpenCasefileDTO openCasefileDTO) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".openRegProc");
        try {
            clientRegProcServices.openRegProc(openCasefileDTO.getOperationId(), openCasefileDTO.getUserOpeCrea());
            return new ResponseEntity<>(true, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error opening RegProc record for: {} with message: {}", openCasefileDTO, e.getMessage());
            return new ResponseEntity<>(false, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    private String getOptional(Optional<String> optional) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".getOptional");
        String toSearch = optional.get();
        toSearch = toSearch.replace("Optional[", "");
        toSearch = toSearch.replace("]", "");
        return toSearch;
    }

    private void updateStatus(Long idClient, String username) {
    //log.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".updateStatus");
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(ESTA_PROC, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi(username);
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }

}
