package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BidIfeValidationRequestDTO implements Serializable{

    private String clavElec;
    private String mrz;
    private String ocr;

}