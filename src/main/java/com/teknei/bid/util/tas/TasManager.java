package com.teknei.bid.util.tas;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Created by Amaro on 31/07/2017.
 */
@Component
public class TasManager {

    private static final String APISERVICE_DOCUMENTS_OPERATION_NAME = "/documents";
    private static final String APISERVICE_FILES_OPERATION_NAME = "/files";

    private static final String PARAMETER_DOCTYPE = "docType";
    private static final String PARAMETER_HEAD_DOCS = "headDocs";
    private static final String PARAMETER_TAIL_DOCS = "tailDocs";
    private static final String PARAMETER_FILES_FILE = "file";
    private static final String PARAMETER_FILES_FILENAME = "fileName";
    private static final String PARAMETER_FILES_CONTENTTYPE = "contentType";
    @Value("${tkn.tas.user}")
    private String tasUser;
    @Value("${tkn.tas.password}")
    private String tasPassword;
    @Value("${tkn.tas.url}")
    private String tasUrl;
    @Value("${tkn.tas.anverse}")
    private String tknTasAnverse;
    @Value("${tkn.tas.face}")
    private String tknTasFace;

    private static final Logger logger = LoggerFactory.getLogger(TasManager.class);

    @PostConstruct
    private void postConstruct() {
    	logger.info("lblancas : [tkn-service-client] :: "+this.getClass().getName()+".postConstruct");
        final String secretUri = "/run/secrets/tas_secret";
        try {
            String content = new String(Files.readAllBytes(Paths.get(secretUri)));
            JSONObject jsonObject = new JSONObject(content);
            if (content == null || content.isEmpty()) {
                logger.error("No secret supplied, leaving default");
            } else {
                String user = jsonObject.optString("username", null);
                String password = jsonObject.optString("password", null);
                String url = jsonObject.optString("url", null);
                if (user != null) {
                    tasUser = user;
                }
                if (password != null) {
                    tasPassword = password;
                }
                if (url != null) {
                    tasUrl = url;
                }
            }
        } catch (IOException e) {
            logger.error("No secret supplied, leaving default");
        }
    }


    /**
     * Crea un documento en TAS asociado al expediente pasado como parámetro
     *
     * @param properties
     * @return
     * @throws Exception
     */
    public JSONObject addDocument(String docType, String headDocs, String tailDocs, Map<String, String> properties, byte[] docBytes, String contentType, String filename) throws Exception {
    	logger.info("lblancas :: [tkn-service-client] :: "+this.getClass().getName()+".addDocument");
        ResponseEntity<String> response = null;
        JSONObject jsonResponse = null;
        try {
            String autStr = tasUser + ":" + tasPassword;
            String authorization = new String(
                    Base64Utils.encodeToString(new String(autStr).getBytes()));

            String URL = new URL(String.format("%s%s", tasUrl, APISERVICE_DOCUMENTS_OPERATION_NAME)).toURI().toString();
            RestTemplate restTemplate = new RestTemplate();

            MultiValueMap<String, Object> requestParameters =
                    new LinkedMultiValueMap<String, Object>();
            requestParameters.add(PARAMETER_DOCTYPE, docType);
            if (headDocs != null)
                requestParameters.add(PARAMETER_HEAD_DOCS, headDocs);
            if (tailDocs != null)
                requestParameters.add(PARAMETER_TAIL_DOCS, tailDocs);
            if (properties != null) {
                for (Map.Entry<String, String> property : properties.entrySet()) {
                    requestParameters.add(property.getKey(), property.getValue());
                }
            }
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Basic " + authorization);
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(requestParameters, headers);
            response = restTemplate.exchange(URL,
                    HttpMethod.POST,
                    requestEntity, String.class);
            jsonResponse = new JSONObject(response.getBody().toString());
            if (jsonResponse.getBoolean("created") == true) {
                if (docBytes != null && docBytes.length > 0) {
                    restTemplate = new RestTemplate();
                    requestParameters =
                            new LinkedMultiValueMap<>();
                    requestParameters.add(PARAMETER_FILES_FILE, new ByteArrayResource(docBytes, filename) {
                        @Override
                        public String getFilename() {
                            return this.getDescription();
                        }
                    });
                    requestParameters.add(PARAMETER_FILES_FILENAME, filename);
                    requestParameters.add(PARAMETER_FILES_CONTENTTYPE, contentType);
                    headers = new HttpHeaders();
                    headers.add("Authorization", "Basic " + authorization);
                    headers.setContentType(MediaType.MULTIPART_FORM_DATA);
                    requestEntity = new HttpEntity<>(requestParameters, headers);
                    URL = new URL(String.format("%s%s/%s", tasUrl, APISERVICE_FILES_OPERATION_NAME, jsonResponse.getString("id"))).toURI().toString();
                    try {
                        response = restTemplate.exchange(URL,
                                HttpMethod.POST,
                                requestEntity, String.class);
                    } catch (RestClientException e) {
                        logger.error("Error {}", e.getMessage());
                        if (e instanceof HttpStatusCodeException) {
                            throw new Exception(e);
                        }
                    }
                    JSONObject fileUploadResponse = new JSONObject(response.getBody().toString());
                    if (fileUploadResponse.getBoolean("file_added") == true) {
                        logger.debug("Archivo subido correctamente. UUID: " + jsonResponse.getString("id"));
                    } else
                        logger.error("Error al insertar el archivo para el UUID: " + jsonResponse.getString("id"));
                }
            }

        } catch (HttpClientErrorException he) {
            logger.error("Status code: {}", he.getStatusCode());
            logger.error("Status message: {}", he.getResponseBodyAsString());
        } catch (RestClientException e) {
            logger.error("Error {}", e);
            logger.info("Error code {}", e.getMessage());
            if (e instanceof HttpStatusCodeException) {
                throw e;
            }
        } catch (MalformedURLException | URISyntaxException e) {
            logger.error("Error {}", e);
            throw e;
        } catch (JSONException e) {
            logger.error("Error {}", e);
            throw e;
        }
        return jsonResponse;
    }

}
