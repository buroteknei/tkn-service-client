package com.teknei.bid;

import com.teknei.bid.controller.rest.BidClieController;
import com.teknei.bid.controller.rest.BidDocuRelController;
import com.teknei.bid.dto.*;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.BidClieTipoRepository;
import com.teknei.bid.persistence.repository.BidEmprClieRepository;
import com.teknei.bid.services.ClientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.*;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class TknServiceClientApplicationTests {

    @Autowired
    private ClientService clientService;
    @Autowired
    private BidClieController bidClieController;
    @Autowired
    private BidEmprClieRepository bidEmprClieRepository;
    @Autowired
    private BidClieTipoRepository bidClieTipoRepository;
    @Autowired
    private BidDocuRelController bidDocuRelController;

    private static final Logger log = LoggerFactory.getLogger(TknServiceClientApplicationTests.class);

    //@Test
    public void testAccomplishment() {
        ResponseEntity<List<StepStatusDTO>> responseEntity = bidClieController.findAccomplishmnet(60l);
        assertNotNull(responseEntity);
        assertEquals(200, responseEntity.getStatusCodeValue());
        responseEntity.getBody().forEach(c -> log.info("Status: {}", c));
    }

    //@Test
    public void testClientdocType() {
        ResponseEntity<List<BidDocuRelDTO>> responseEntity = bidDocuRelController.findActiveDocumentRelations();
        assertEquals(200, responseEntity.getStatusCodeValue());
        assertNotNull(responseEntity.getBody());
        responseEntity.getBody().forEach(i -> log.info("Retrieved: {}", i));
    }

    //@Test
    public void testContextLoad() {
    }

    //@Test
    public void testFinders(){
        Long idCustomer = 233l;
        String mail = "garvenruben1@gmail.com";
        String curp = "AAAA900101HTLBBB10";
        String tel = "2411197777";
        ResponseEntity<ClientDetailDTO> responseEntityId = bidClieController.findDetail(Optional.of(idCustomer), Optional.ofNullable(null), Optional.ofNullable(null), Optional.ofNullable(null));
        ResponseEntity<ClientDetailDTO> responseEntityCurp = bidClieController.findDetail(Optional.ofNullable(null), Optional.of(curp), Optional.ofNullable(null), Optional.ofNullable(null));
        ResponseEntity<ClientDetailDTO> responseEntityMail = bidClieController.findDetail(Optional.ofNullable(null), Optional.ofNullable(null), Optional.of(mail), Optional.ofNullable(null));
        ResponseEntity<ClientDetailDTO> responseEntityTel = bidClieController.findDetail(Optional.ofNullable(null), Optional.ofNullable(null), Optional.ofNullable(null), Optional.of(tel));
        log.info("Response by id: {}", responseEntityId);
        log.info("Response by curp: {}", responseEntityCurp);
        log.info("Response by mail: {}", responseEntityMail);
        log.info("Response by tel: {}", responseEntityTel);
    }

    //@Test
    public void testInitCasefile() {
        OpenCasefileDTO casefileDTO = new OpenCasefileDTO();
        casefileDTO.setOperationId(1l);
        casefileDTO.setUsername("amaro");
        bidClieController.openCasefile(casefileDTO);
    }

    //@Test
    public void saveCustomer() {
        log.info("Find opened for today");
        log.info("Opened: {}", clientService.findOpenedProcessForToday());
        //List<String> listOpened = clientService.findOpenedProcessForToday();
        //listOpened.forEach(c -> log.info("Found: {}", c));
    }

    //@Test
    //@Transactional
    public void testAddCustomer() {
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setEmprId(1l);
        clientDTO.setCurp("AACJ881203HMCMRR08");
        clientDTO.setEmail("amaro.coria@gmail.com");
        //clientDTO.setIdClientType(1);
        clientDTO.setName("JORGE");
        clientDTO.setSurnameFirst("AMARO");
        clientDTO.setSurnameLast("CORIA");
        ClieTelDTO clieTelDTO = new ClieTelDTO();
        ClieCelDTO clieCelDTO = new ClieCelDTO();
        clieCelDTO.setTel("5540812022");
        clientDTO.setTelephones(Arrays.asList(clieCelDTO));
        clientDTO.setUsername("test");
        /*
        ResponseEntity<ClientDTO> responseEntity = bidClieController.addClient(clientDTO);
        assertEquals(200, responseEntity.getStatusCodeValue());
        ClientDTO savedInstance = responseEntity.getBody();
        assertNotNull(savedInstance.getId());
        log.info("Saved instance: {}", savedInstance);
        Long savedId = savedInstance.getId();
        ResponseEntity<ClientDTO> duplicatedResponseEntity = bidClieController.addClient(clientDTO);
        assertEquals(422, duplicatedResponseEntity.getStatusCodeValue());
        ResponseEntity<ClientDetailDTO> responseDetail = bidClieController.findDetail(Optional.of(savedId), Optional.ofNullable(null), Optional.ofNullable(null), Optional.ofNullable(null));
        assertEquals(200, responseDetail.getStatusCodeValue());
        ClientDetailDTO detailDTO = responseDetail.getBody();
        assertEquals(clientDTO.getName(), detailDTO.getName());
        BidEmprCliePK emprCliePK = new BidEmprCliePK();
        emprCliePK.setIdClie(savedId);
        emprCliePK.setIdEmpr(1l);
        BidEmprClie emprClie = bidEmprClieRepository.findOne(emprCliePK);
        log.info("Relation Empr - Clie : {}", emprClie);
        assertNotNull(emprClie);
        BidClieTipoPK bidClieTipo = new BidClieTipoPK();
        bidClieTipo.setIdClie(savedId);
        bidClieTipo.setIdTipoClie(1);
        BidClieTipo tipoClie = bidClieTipoRepository.findOne(bidClieTipo);
        log.info("Relation Clie - TipoClie: {}", tipoClie);
        assertNotNull(tipoClie);
        clientService.updateProcessStatus(savedId, "user1");
        */
    }

    //@Test
    //@Transactional
    public void testAccount() {
    	
        BidCreditInstitutionRequest request = new BidCreditInstitutionRequest();
        request.setName("TKNBANK");
        request.setDesc("Banco de la alegria");
        request.setUsername("amaro");
        ResponseEntity<BidInstCred> responseEntity = bidClieController.saveInstCred(request);
        assertEquals(200, responseEntity.getStatusCodeValue());
        BidInstCred bidInstCred = responseEntity.getBody();
        Long idRegistered = bidInstCred.getIdInstCred();
        BidClientAccountDestinyDTO requestAccount = new BidClientAccountDestinyDTO();
        /*
        requestAccount.setActive(true);
        requestAccount.setAlias("PAPITAS");
        requestAccount.setAmmount(10000l);
        requestAccount.setClabe("12345");
        requestAccount.setIdClient(1l);
        requestAccount.setIdCreditInstitution(idRegistered);
        requestAccount.setNewIndicator(true);
        requestAccount.setUsername("amaro");
        ResponseEntity<BidClieCtaDest> responseEntityAccount = bidClieController.saveCtaDest(requestAccount);
        assertEquals(200, responseEntityAccount.getStatusCodeValue());
        BidClieCtaDest ctaDest = responseEntityAccount.getBody();
        log.info("Account created: {}", ctaDest);
        BigDecimal bd1 = new BigDecimal(100);
        assertEquals(ctaDest.getMontMax().compareTo(bd1), 0);
        ResponseEntity<List<BidClieCtaDest>> responseEntityAccountList = bidClieController.findRelatedAccounts(1l);
        assertEquals(200, responseEntityAccountList.getStatusCodeValue());
        List<BidClieCtaDest> listActives = responseEntityAccountList.getBody();
        assertThat(listActives, containsInAnyOrder(ctaDest));
        BidClientAccountDestinyDTO requestAccountUpdate = new BidClientAccountDestinyDTO();
        requestAccountUpdate.setActive(false);
        requestAccountUpdate.setAlias("MANDADOS");
        requestAccountUpdate.setAmmount(19000l);
        requestAccountUpdate.setClabe("12345");
        requestAccountUpdate.setIdClient(1l);
        requestAccountUpdate.setIdCreditInstitution(idRegistered);
        requestAccountUpdate.setNewIndicator(false);
        requestAccountUpdate.setUsername("mayo");
        ResponseEntity<BidClieCtaDest> responseEntityAccountUpdated = bidClieController.saveCtaDest(requestAccountUpdate);
        assertEquals(200, responseEntityAccountUpdated.getStatusCodeValue());
        BidClieCtaDest updated = responseEntityAccountUpdated.getBody();
        log.info("Account updated: {}", updated);
        BigDecimal bd2 = new BigDecimal(190);
        assertEquals(bd2.compareTo(updated.getMontMax()), 0);
        ResponseEntity<List<BidClieCtaDest>> responseEntityListInactive = bidClieController.findRelatedAccountsInactive(1l);
        assertEquals(200, responseEntityListInactive.getStatusCodeValue());
        List<BidClieCtaDest> listInactive = responseEntityListInactive.getBody();
        assertThat(listInactive, containsInAnyOrder(updated));*/
    }

    //@Test
    // @Transactional
    public void testCreditInst() {
    	
        BidCreditInstitutionRequest request = new BidCreditInstitutionRequest();
        request.setName("TKNBANK");
        request.setDesc("Banco de la alegria");
        request.setUsername("amaro");
        ResponseEntity<BidInstCred> responseEntity = bidClieController.saveInstCred(request);
        assertEquals(200, responseEntity.getStatusCodeValue());
        BidInstCred bidInstCred = responseEntity.getBody();
        String name = bidInstCred.getNomCort();
        String desc = bidInstCred.getNomLarg();/*
        Long idRegistered = bidInstCred.getIdInstCred();
        log.info("Entity registered: {}", bidInstCred);
        assertEquals(request.getName(), name);
        assertEquals(request.getDesc(), desc);
        ResponseEntity<List<BidInstCred>> listResponseEntity = bidClieController.findInstCred();
        List<BidInstCred> savedList = listResponseEntity.getBody();
        Integer size = savedList.size();
        assertEquals(200, listResponseEntity.getStatusCodeValue());
        assertThat(savedList, containsInAnyOrder(bidInstCred));
        BidCreditInstitutionRequest updateRequest = new BidCreditInstitutionRequest();
        updateRequest.setUsername("amaro2");
        updateRequest.setName("TKNBANKU");
        updateRequest.setDesc("Banco SS");
        updateRequest.setActive(false);
        updateRequest.setId(idRegistered);
        ResponseEntity<BidInstCred> responseEntityUpdate = bidClieController.saveInstCred(updateRequest);
        assertEquals(200, responseEntityUpdate.getStatusCodeValue());
        BidInstCred bidInstCredUpdated = responseEntityUpdate.getBody();
        log.info("Entity updated: {}", bidInstCredUpdated);
        assertNotEquals(name, bidInstCredUpdated.getNomCort());
        assertEquals(updateRequest.getName(), bidInstCredUpdated.getNomCort());
        assertEquals(updateRequest.getDesc(), bidInstCredUpdated.getNomLarg());
        assertEquals(2, bidInstCredUpdated.getIdEsta());
        ResponseEntity<List<BidInstCred>> listResponseEntityUpdated = bidClieController.findInstCred();
        assertEquals(200, listResponseEntityUpdated.getStatusCodeValue());
        Integer size2 = listResponseEntityUpdated.getBody().size();
        assertEquals(size, size2);*/
    }

}
